# metrics-platform-client-php

Metrics client library for PHP, intended for use with MediaWiki.

See the [Metrics Platform](https://wikitech.wikimedia.org/wiki/Metrics_Platform) project page on Wikitech for background.

## Testing
Run tests with `composer test`. The test output will include a coverage report.

